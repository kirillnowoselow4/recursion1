def pow(x, n):
    if n == 0:
        return 1
    elif n > 0:
        a = x * pow(x, n - 1)
        return a
    else:
        a = 1 / x * pow(x, n + 1)
        return a


print(pow(5, -1))