from main import pow
import pytest

@pytest.mark.parametrize("x, n, expected_result", [(2, 4, 16),
                                                   (-3, 3, -27),
                                                   (5, -1, 0.2),
                                                   (-4, -1, -0.25),
                                                       (2, 0, 1)])
def test_pow1(x, n, expected_result):
    assert pow(x, n) == expected_result

def test_type_error():
    with pytest.raises(TypeError):
        assert pow(10,"g")